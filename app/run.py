from flask import Flask, render_template, request, redirect, url_for, jsonify

# import pyodbc
# from flask import Flask

# app = Flask(__name__)
#
# #conn = pyodbc.connect('DRIVER={Sybase ASE ODBC Driver};SERVER=hostname;DATABASE=db_name;UID=username;PWD=password')
# #conn = pyodbc.connect('DRIVER={Sybase ASE ODBC Driver};SERVER=hostname;DATABASE=db_name;UID=username;PWD=password')
#
# # Configura los parámetros de la conexión
# params = {
#     'DSN': 'SYSBASES STANBY',
#     'UID': 'dvvasquez',
#     'PWD': 'dvv.0126',
#     'DATABASE': 'cob_ahorros',
#     'SERVER': '195.160.1.3'
# }
#
# # Establece la conexión utilizando los parámetros configurados
# conn = pyodbc.connect(';'.join([f'{k}={v}' for k, v in params.items()]))
#
#
# @app.route('/')
# def index():
#     cursor = conn.cursor()
#     cursor.execute('SELECT top 3 * FROM cob_ahorros..ah_cuenta')
#     rows = cursor.fetchall()
#     conn.close()
#     return str(rows)
#
# if __name__ == '__main__':
#     app.run()


app = Flask(__name__)
# @app.route('/')
# def hello_world():
#     return '<h1> Hello, World XD! </h1>'

@app.route('/')
def inicio():
    return render_template('public/index.html')


if __name__ == '__main__':
    #app.run()
    app.run(debug = True, port =5000)
#python -m pip freeze > requirements.txt